A few years ago, I started work on [Flash][1], a project dashboard tool to
display not just CI status but other useful information about a given project:
backlog status, CI progress, latest commits, etc. Now I'm thinking about a new
version, written in JavaScript rather than Python: **Hailstorm**.

The goals for this project are as follows:

 - [ ] **Easily extensible**: it should be relatively easy to add new services
    to Hailstorm. With Flash all of the services were defined in [a separate
    repo][2], which meant this repository needed to be forked or cloned locally
    to add custom services. To make it easier, services will be provided to
    Hailstorm on a modular plug-in basis. The kinds of services Hailstorm is
    being designed to support are:
    
     - [ ] Version control (e.g. GitLab, GitHub)
    
     - [ ] CI/CD (e.g. Travis, Codeship)
     
     - [ ] Code quality (e.g. Coveralls)
     
     - [ ] Project/backlog status (e.g. Pivotal Tracker)

 - [ ] **Progressively enhanced**: dashboards aren't always viewed from the
    most straightforward browser setup, so Hailstorm doesn't require client-side
    JavaScript for basic functionality. Compatibility with older browsers will
    be considered for corporate deployments where evergreen browsers aren't
    always available.
 
 - [ ] **Easily deployed**: set up for low-effort configuration and deployment
    via Cloud Foundry or a Docker container.

  [1]: https://github.com/textbook/flash
  [2]: https://github.com/textbook/flash_services
